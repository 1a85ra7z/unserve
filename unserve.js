const fs = require('fs');

module.exports = () => {
    let plugins = {};
    let _ = {};

    return {
        use: (path, options, callback) => {
            fs.readdir(path, (e, d) => {
                if(!e ? (d.includes('unserve.json')) : false) {
                    fs.readFile(`${path}/unserve.json`, 'UTF-8', (er, da) => {
                        if(!er ? (typeof da === 'string' ? (da.length > 0) : false) : false) {
                            let data;

                            try {
                                data = JSON.parse(da);
                            } catch (err) {
                                console.error(err);
                            }

                            if(data) {
                                if(typeof data.name !== 'string') {
                                    console.error('No name in unserve.json!');
                                }

                                if(typeof data.version !== 'string') {
                                    console.error('No entry version in unserve.json!');
                                }

                                if(typeof data.entry !== 'string') {
                                    console.error('No entry point in unserve.json!');
                                }

                                if(
                                    typeof data.name === 'string' &&
                                    typeof data.version === 'string' &&
                                    typeof data.entry === 'string'
                                ) {
                                    if(fs.existsSync(`${path}/${data.entry}`)) {
                                        let cpg = require(`${path}/${data.entry}`);

                                        if(typeof cpg === 'function') {
                                            let ac = {};

                                            if(typeof data.dependencies === 'object') {
                                                data.dependencies.forEach(rq => {
                                                    if(plugins[rq] !== undefined) {
                                                        ac[rq] = plugins[rq]._;
                                                    }
                                                });
                                            }

                                            if(typeof data.dependencies === 'object' ? (Object.keys(ac).length === Object.keys(data.dependencies).length) : true) {
                                                plugins[data.name] = {
                                                    path: path,
                                                    name: data.name,
                                                    version: data.version,
                                                    _: {
                                                        _: {},
                                                        ...cpg(options, ac)
                                                    }
                                                };

                                                plugins[data.name]._._.options = {
                                                    ...plugins[data.name]._._.options,
                                                    ...options
                                                };
                                                _[data.name] = plugins[data.name]._;

                                                if(typeof callback === 'function') {
                                                    callback();
                                                }
                                            } else {
                                                console.error(`Did not fulfill all requirements for ${data.name}!`);
                                            }
                                        }
                                    } else {
                                        console.error(`Path to plugin doesn't exist!`)
                                    }
                                }
                            }
                        }
                    });
                }
            });
        },

        _: _
    }
};