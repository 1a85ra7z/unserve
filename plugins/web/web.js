const https = require('https');
const web = require('http');

const url = require('url');
const path = require('path');
const fs = require('fs');

const onceupon = require('onceupon.js')();

module.exports = (options) => {
    let opt = {
        ssl: {
            data: {
                key: '',
                cert: '',
            },

            enabled: false
        },

        root: './',

        ports: {
            http: 8080,
            https: 443
        }
    };

    if(options.ssl !== undefined ? (options.ssl.key !== undefined && options.ssl.cert !== undefined) : false) {
        opt.ssl = {
            data: {
                key: fs.readFileSync(options.ssl.key).toString(),
                cert: fs.readFileSync(options.ssl.cert).toString(),
            },

            enabled: true
        }
    }

    if(options.ports === 'object') {
        if(typeof options.ports.http === 'number') {
            opt.ports.http = options.ports.http;
        }

        if(typeof options.ports.https === 'number') {
            opt.ports.https = options.ports.https;
        }
    }

    if(typeof options.root === 'string') {
        opt.root = options.root;
    }

    onceupon.create('connection');

    let exceptions = [];
    let ecnt = false;

    let _https;
    let _http;

    let h = (req, res) => {
        if(!ecnt) {
            let fexpt = -1;

            exceptions.forEach((cext, iext) => {
                if(cext.u.test(req.url)) {
                    fexpt = iext;
                }
            });

            if(fexpt === -1) {
                let f = path.join(process.cwd(), opt.root, url.parse(req.url).pathname);

                fs.exists(f, (e) => {
                    if(!e) {
                        res.writeHead(404, {'Content-Type': 'text/plain'});
                        res.write('404 - Not found');
                        res.end();

                        return;
                    }

                    if(fs.statSync(f).isDirectory()) f += '/index.html';

                    fs.readFile(f, 'binary', (err, d) => {
                        if(err) {
                            res.writeHead(500, {'Content-Type': 'text/plain'});
                            res.write(err + '\n');
                            res.end();

                            return;
                        }

                        res.writeHead(200);
                        res.write(d, 'binary');
                        res.end();
                    });
                });
            } else {
                exceptions[fexpt].c({
                    url: req.url,

                    write: (data, type) => {
                        res.writeHead(exceptions[fexpt].h.n, exceptions[fexpt].h.o);
                        res.write(data !== undefined ? data : '', type !== undefined ? type : 'binary');
                        res.end();
                    },

                    head: (status, header) => {
                        if(typeof status === 'number') {
                            exceptions[fexpt].h.n = status;
                        }

                        if(typeof header === 'object') {
                            exceptions[fexpt].h.o = header;
                        }
                    }
                });
            }
        } else {
            res.end();
            req.connection.end();
            req.connection.destroy();
        }
    };

    if(opt.ssl.enabled) {
        _https = https.createServer(opt.ssl.data, h);

        _https.listen(
            opt.ports.https
        );
    }

    _http = web.createServer(h);
    _http.listen(opt.ports.http);

    return {
        except: (url, callback) => {
            exceptions.push({
                u: new RegExp(url.replace(/\*/g, '\\w\*')),
                c: callback,
                h: {
                    n: 200,
                    o: {}
                }
            })
        },

        close: callback => {
            let cls = 0;
            ecnt = true;

            let clb = () => {
                cls++;

                if(cls === 2) {
                    callback();
                }
            };

            _http.close(() => {
                clb();
            });

            if(opt.ssl.enabled) {
                _https.close(() => {
                    clb();
                });
            }
        },

        on: onceupon.on,
        once: onceupon.once,

        _: {
            http: _http,
            https: _https,
            options: opt
        }
    };
};