module.exports = (options, requirements) => {
    if(typeof requirements === 'object' ? (typeof requirements.web === 'object' && typeof requirements.cdn === 'object') : false) {
        requirements.cdn.locate(
            'version.hook', 'https://gitlab.com/1a85ra7z/hook.js/raw/master/version.hook'
        );

        requirements.cdn.locate(
            'hooker.min.js', 'https://gitlab.com/1a85ra7z/hook.js/raw/master/hooker.min.js'
        );

        requirements.cdn.locate(
            'hook.min.js', 'https://gitlab.com/1a85ra7z/hook.js/raw/master/hook.min.js'
        );
    }
};