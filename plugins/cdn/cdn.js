const https = require('https');

let f = (p, c) => {
    https.get(p, (r) => {
        let d = '';

        r.on('data', (c) => {
            d += c;
        });

        r.on('end', () => {
            c(d);
        });

    }).on('error', (e) => {
        console.error(e);
    });
};

module.exports = (options, requirements) => {
    if(typeof requirements === 'object' ? (typeof requirements.web === 'object') : false) {
        let r = (url, location) => {
            f(
                location,
                d => {
                    requirements.web.except(url, (q) => {
                        q.write(d);
                    });
                }
            )
        };

        Object.keys(options).forEach(k => {
            r(k, options[k]);
        });

        return {
            locate: r
        };
    }
};