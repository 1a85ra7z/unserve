const WebSocket = require('ws');
const onceupon = require('onceupon.js');

module.exports = (options, required) => {
    if(required !== undefined ? (required.web !== undefined) : false) {
        const app = required.web._.options.ssl.enabled ? require('https') : require('http');

        let opt = {
            port: 3000
        };

        if(typeof options === 'object') {
            if(typeof options.port === 'number') {
                opt.port = options.port;
            }
        }

        let ou = onceupon();
        ou.create('connection');

        let h = app.createServer(required.web._.options.ssl.enabled ? required.web._.options.ssl.data : null);
        h.listen(opt.port);

        let wss = new WebSocket.Server({
            server: h
        });

        wss.on('connection', (ws) => {
            let cis;
            let gr = false;
            let wsou = onceupon();

            let wsr = {
                on: wsou.on,
                once: wsou.once,

                emit: (event, text) => {
                    ws.send(`${event}::${typeof text}::${text !== undefined ? (typeof text === 'object' ? JSON.stringify(text) : text.toString()) : ''}`);
                },

                close: () => {
                    wsou.fire('disconnected');
                    ws.terminate();
                },

                speed: -1
            };

            ou.fire('connection', wsr);

            ws.addEventListener('message', (m) => {
                let f = m.data.split('::');
                let l = '';

                switch(f[1]) {
                    case 'object':
                        l = JSON.parse(f[2]);
                        break;
                    case 'string':
                        l = f[2];
                        break;
                    case 'number':
                        l = parseInt(f[2]);
                        break;
                    case 'boolean':
                        l = (f[2] === 'true');
                        break;
                    default:
                        l = null;
                        break;
                }

                wsou.fire(f[0], l === '' ? null : l);
            });

            let ssp = () => {
                wsr.emit('ping');

                cis = new Date();
                gr = false;

                setTimeout(() => {
                    if(!gr) {
                        wsr.close();
                        clearInterval(ci);
                        wsou.fire('timeout');
                    }
                }, 10000);
            };

            wsr.on('pong', () => {
                gr = true;

                wsr.speed = new Date() - cis;
            });

            let ci = setInterval(ssp, 15000);

            ssp();
        });

        return {
            on: ou.on,
            once: ou.once
        }
    }
};