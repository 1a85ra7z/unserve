let app = require('unserve')();

app.use('./plugins/web', {
    root: './http'
}, () => {
    app.use('./plugins/socket');

    app.use('./plugins/cdn', {}, () => {
        app.use('./plugins/hook.js');
    });
});